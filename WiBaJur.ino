#define SENSOR_PIN D0
#define MIC_PIN D6
#define LED_PIN  D5

#define SENSOR_OFF 0
#define SENSOR_ON  1

#define LED_OFF     0
#define LED_ON      1
#define LED_TIMED   2
#define LED_TURN_TIMED 3
#define LED_TURN_ON    4
#define LED_TURN_OFF   5

#define LED_PWM_STEP  5
#define LED_PWM_TIME  30
#define LED_PWM_MAX 255

#define CLAP_IDLE  0
#define CLAP_PULSE 1
#define CLAP_PAUSE 2
#define CLAP_COUNT 3
#define CLAP_END   4

#define CLAP_LEVEL 10
#define AD_SAMPLES 16
int led_status = 0;
int led_level = 0;
long led_time = 0;
int sensor_status = 0;
long sensor_time = 0;
int mic_val = 0;
int ad_val = 0;
int clap_status = 0;
int clap_count = 0;
long clap_pulse = 0;
long clap_time = 0;
int ad_samples[32];
int ad_sample;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(SENSOR_PIN, INPUT);
  pinMode(MIC_PIN, INPUT);
  pinMode(LED_PIN, OUTPUT);
  Serial.begin(115200);
}

void loop() {
  digitalWrite(LED_BUILTIN, !digitalRead(SENSOR_PIN));   // Led as feedback of motion
  led_control();
  sensor_control();
  clap_control();
}

void led_control(void) {
  switch (led_status) {
    case LED_OFF:
      if ((led_level >= LED_PWM_STEP) && (millis() > led_time+LED_PWM_TIME)){
        led_level-=LED_PWM_STEP;
        analogWrite(LED_PIN,led_level);
        led_time = millis();
        if (led_level == 0){
          digitalWrite(LED_PIN,LOW);
          Serial.println("Fim PWM Off");
        }
      }          
      break;
    case LED_ON:
      if ((led_level < LED_PWM_MAX) && (millis() > led_time+LED_PWM_TIME)){
        led_level+=LED_PWM_STEP;
        led_time = millis();
        analogWrite(LED_PIN,led_level);
        if (led_level == LED_PWM_MAX){
          digitalWrite(LED_PIN,HIGH);
          Serial.println("Fim PWM On");
        }
      }                    
      break;
    case LED_TURN_ON:
      led_level = 0;
      led_time = millis();
      led_status = LED_ON;
      break;
    case LED_TURN_OFF:
      led_level = LED_PWM_MAX;
      led_time = millis();
      led_status = LED_OFF;
      break;
    case LED_TURN_TIMED:
      digitalWrite(LED_PIN,HIGH);
      led_time = millis();
      led_status = LED_TIMED;        
      break;      
    case LED_TIMED:
      if (millis() == led_time+30000){
        digitalWrite(LED_PIN,LOW);
        led_status = LED_OFF;        
        Serial.println("LED Timeout");
      }
      break;    
   }
}

void clap_control(void){
int i;

  ad_val = analogRead(A0);
  ad_samples[ad_sample%AD_SAMPLES] = ad_val;
  ad_sample++;
  mic_val = 0;
  for (i=0;i<AD_SAMPLES;i++){
    mic_val += ad_samples[i]; 
  }
  mic_val = mic_val / AD_SAMPLES;
 if (ad_val - mic_val >= CLAP_LEVEL){
    Serial.print("Ad-Val=");
    Serial.print(ad_val);
    Serial.print(",");
    Serial.println(mic_val);
  }
  switch(clap_status){
    case CLAP_IDLE:
       if (ad_val-mic_val >= CLAP_LEVEL){
         clap_status = CLAP_PULSE;
         clap_count = 0;
         clap_time = micros();              
       }
       break;
    case CLAP_PULSE:
       if (ad_val-mic_val < CLAP_LEVEL){
          clap_time = micros()-clap_time;
          Serial.print("Clap Pulse:");
          Serial.println(clap_time);
          if ((clap_time > 0) && (clap_time < 500)){
             clap_count++;
             clap_status = CLAP_PAUSE;                          
             clap_time = micros();              
          }          
          else{
             clap_status = CLAP_IDLE;                          
             Serial.println("Clap False");            
          }
       }
       break;
    case CLAP_PAUSE:
       if ((micros() >= clap_time) && (micros() >= clap_time+2000)){
           clap_status = CLAP_COUNT;        
       }
       break;
    
    case CLAP_COUNT:
      if (ad_val-mic_val > CLAP_LEVEL){
         clap_status = CLAP_PULSE;
         clap_time = micros();              
      }
      else if ((micros() >= clap_time) && (micros() >= clap_time +1000000)){
         clap_status = CLAP_END;        
      }
      break;
    case CLAP_END:
      Serial.print("Clap End-Clapse:");
      Serial.println(clap_count);
      clap_status = CLAP_IDLE;
      if (clap_count == 2){
         if (led_status == LED_OFF){
          led_status = LED_TURN_ON;
         }
         else led_status = LED_TURN_OFF;
      }
      break;           
  } 
}

void sensor_control(void) {
  switch (sensor_status) {
    case SENSOR_OFF:
      if (digitalRead(SENSOR_PIN) == 1) {
        Serial.println("Movimento");
        sensor_status = SENSOR_ON;
        sensor_time = millis();
      }
      break;
    case SENSOR_ON:
      if (digitalRead(SENSOR_PIN) == 0) {
        Serial.println("Fim movimento");
        sensor_status = SENSOR_OFF;
        break;
      }
      // Movimento de 500ms (liga/desliga lampada temporizada)
      if (millis() == sensor_time + 500) {
        sensor_time--;       
        if (led_status == LED_OFF) {
          led_status = LED_TURN_TIMED; 
          Serial.println("LED Timed");
        }
        else{
          led_status = LED_TURN_OFF;
          Serial.println("LED Time Off");          
        }
      }
      // Movimento por 3s Turn ON
      if (millis() == sensor_time + 4000) {
        sensor_time--;       
        led_status = LED_TURN_ON;
        digitalWrite(LED_PIN,LOW);
        delay(300);
        Serial.println("LED On");          
      }
  }
}

